const bcrypt = require('bcrypt');

const saltRounds = 10;

const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

bcrypt.hash(myPlaintextPassword, saltRounds, (err, hash) => {
    if (err) console.error(err);
    // console.log(hash);
});

// const hash = "$2b$10$.aFD.RNaAn9v1xsFF9qsf.nA7P1BYvJ7i7CAUZM3QlJ0ywRaa3cia"
// const hash = "$2b$10$6pP3BHS5k/qvtACw0cVBYOyMbrWDyH1ooPYZ7/5oMp4yobyvUQtZq"
const hash = "$2b$10$2nIzGTBKK0brwWPCFXPAjeM47KmiIUU6A9jG5xaiAgQs0DXj0rGC2"

bcrypt.compare(myPlaintextPassword, hash, (err, result) => {
    if (err) console.error(err);
    console.log(`[0] ${result}`);
});
bcrypt.compare(someOtherPlaintextPassword, hash, (err, result) => {
    if (err) console.error(err);
    console.log(`[1] ${result}`);
});