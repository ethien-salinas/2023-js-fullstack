require('dotenv').config()

// 1 - importar la dependencia
var jwt = require('jsonwebtoken')

// 2 - generar el jwt con su 'payload' y palabra clave
var token = jwt.sign({
    // exp: Math.floor(Date.now() / 1000) + (60 * 60),
    name: 'Ethien Salinas'
},
    process.env.JWT_SECRET,
    { expiresIn: '1m' }
)
console.log(token)

// *** *** *** *** Verificar un JWT *** *** *** ***

// verify a token symmetric - synchronous
var decoded
try {
    decoded = jwt.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRXRoaWVuIFNhbGluYXMiLCJpYXQiOjE2ODcwMjU0NzgsImV4cCI6MTY4NzAyNTUzOH0.e3WMvLrvUyYfO1JKPB7coan9Yri_z1MlKOQQEVZhHXE', process.env.JWT_SECRET)
    console.log(JSON.stringify(decoded))
} catch (error) {
    console.log(error);
}
