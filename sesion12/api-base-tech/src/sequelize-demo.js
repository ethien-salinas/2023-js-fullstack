require('dotenv').config()
const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD, {
  host: 'localhost',
  dialect: 'postgres',
});

const User = sequelize.define('User', {
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING
  }
}, {
});

(async () => {
  try {
    // await sequelize.authenticate()
    console.log('Connection has been established successfully.')
    User.sync()
  } catch (error) {
    console.error('Unable to connect to the database:', error)
  }
})()
