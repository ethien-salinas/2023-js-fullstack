import { logger } from "../logger"

export const logRequestMiddleware = function (req, res, next) {
  logger.debug(`[${req.trace_id}][${req.originalUrl}] headers: ${JSON.stringify(req.headers)}`)
  logger.debug(`[${req.trace_id}][${req.originalUrl}] params: ${JSON.stringify(req.params)}`)
  const { password, ...rest } = req.body
  logger.debug(`[${req.trace_id}][${req.originalUrl}] body: ${JSON.stringify(rest)}`)
  next()
}