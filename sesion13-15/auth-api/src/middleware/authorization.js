import { verify } from "jsonwebtoken"
import { logger } from "../logger"

export const authorizationMiddleware = (req, res, next) => {

  let token = req.headers.authorization
  if (!token) {
    res
      .status(401)
      .json({
        error: "Unauthorized",
        code: "AUTH1001",
        msg: "missing token"
      })
  } else {
    try {
      token = token.split(" ")[1]
      const decoded = verify(token, process.env.JWT_SECRET)
      const paylod = JSON.parse(JSON.stringify(decoded))
      logger.info(`[${req.trace_id}] El usuario ${paylod.id} ha consultado la ruta ${req.originalUrl}`)
      next()
    } catch (error) {
      res
        .status(401)
        .json({
          error: "Unauthorized",
          code: "AUTH1002",
          msg: error.message
        })
    }
  }
}