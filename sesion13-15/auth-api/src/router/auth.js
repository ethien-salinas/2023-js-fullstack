import { compare, hash } from "bcrypt";
import express from "express";
import { sign } from "jsonwebtoken";
import { User } from "../db/model/User";
import { logger } from "../logger";

export const router = express.Router()

const saltRounds = 10;

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})

router.get('/', (req, res) => {
  res.send('Root Auth service')
})

router.post('/login', async (req, res) => {
  const { email, password } = req.body
  console.log(req.trace_id, email, password);
  const user = await User.findOne({
    where: { email }
  });

  logger.info(`[${req.trace_id}] Usuario ${user.id} encontrado`)
  if (user && await compare(password, user.password)) {
    const payload = {
      id: user.id,
      name: user.firstName,
      lastname: user.lastName,
      email
    }
    const token = sign(payload, process.env.JWT_SECRET, { expiresIn: '3h' });
    logger.info(`[${req.trace_id}] El usuario ${user.id} a accedido al sistema`)
    res.json({ token })
  }
  else {
    logger.error(`[${req.trace_id}] Credenciales inválidas para ${email}`)
    res
      .status(401)
      .json({
        error: "Unauthorized",
        code: "USRLG1001"
      })
  }

})

router.post('/register', async (req, res) => {
  let { name, lastname, email, password } = req.body

  let hashedText = ""
  let dbResult = null
  try {
    hashedText = await hash(password, saltRounds)
    dbResult = await User.create({
      firstName: name,
      lastName: lastname,
      email,
      password: hashedText
    })

    logger.info(`[${req.trace_id}][register] Se ha registrado al usuario ${dbResult.id}`)
    if (dbResult) {
      const payload = {
        id: dbResult.id,
        name,
        lastname,
        email
      }
      const token = sign(payload, process.env.JWT_SECRET, { expiresIn: '3h' });
      res.send(token)
    }

  } catch (error) {
    logger.error(`[${req.trace_id}][register] ${JSON.stringify(error)}`)
    res.send(`[${req.trace_id}] Error al registrar al usuario ${email}`)
  }

})