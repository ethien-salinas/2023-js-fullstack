### Root
GET http://localhost:4000/ HTTP/1.1


### Public route
GET http://localhost:4000/public HTTP/1.1

### Protected route
GET http://localhost:4000/protected HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjE2YWU1M2ZmLTVmMjMtNGM3OS1hMmQ5LTNkYjI0MWE3MDNlNSIsIm5hbWUiOiJldGhpZW4iLCJsYXN0bmFtZSI6InNhbGluYXMiLCJlbWFpbCI6ImV0aGllbi5zYWxpbmFzKzBAZ21haWwuY29tIiwiaWF0IjoxNjg4ODQ5NDk5LCJleHAiOjE2ODg4NDk1Mjl9.rGPAu9fC8z1R7J9SWOwPTZV77OlbYAaqvg3mDAgjDh4

### Auth root
GET http://localhost:4000/auth HTTP/1.1

### Auth login
POST http://localhost:4000/auth/login HTTP/1.1
Content-Type: application/json

{
  "email": "ethien.salinas+0@gmail.com",
  "password": "qwerty"
}

### Auth register
POST http://localhost:4000/auth/register HTTP/1.1
Content-Type: application/json

{
  "name": "ethien",
  "lastname": "salinas",
  "email": "ethien.salinas+0@gmail.com",
  "password": "qwerty"
}