const winston = require('winston');
require('winston-daily-rotate-file')

const logger = winston.createLogger({
  level: process.env.LOGGER_LEVEL,
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZ' }),
    winston.format.printf(({ timestamp, service, level, message }) => `${timestamp} | ${service} | ${level} | ${message}`)
  ),
  defaultMeta: { service: 'api-base-tech' },
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.DailyRotateFile({
      level: 'error',
      filename: './logs/error-%DATE%.log',
      // datePattern: 'YYYY-MM-DD-HH-mm',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      utc: false,
    }),
    new winston.transports.DailyRotateFile({
      filename: './logs/application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      utc: false,
    })
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console());
}

module.exports = logger