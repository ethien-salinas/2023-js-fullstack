import { defineStore } from "pinia"
import { router } from "../router";

export const useAuthStore = defineStore('authStore', {

  state: () => ({
    user: {},
    token: ''
  }),

  getters: {
    isAuthenticated() {
      return !!this.token
    }
  },

  actions: {
    login(email, password) {
      console.log('[login]🍍🍍🍍');
      if (email === "ethien.salinas@gmail.com" && password === "qwerty") {
        console.log("*** usuario válido 😎");
        this.token = "fake.jwt.token"
        this.user.id = "ac23c220-e66d-47b6-ba7d-cfedd1948143"
        this.user.name = "Ethien"
        this.user.lastname = "Salinas"
        router.push({ name: 'home' })
      } else {
        console.log("*** no sé quien eres 🤔");
      }
    },

    logout() {
      this.user = {}
      this.token = ''
      router.push({ name: 'login' })
    }
  }

})