import { defineStore } from 'pinia'
import { router } from '../router'

export const useAuthStore = defineStore('authStore', {

  state: () => ({
    user: {},
    token: localStorage.getItem('token') || ''
  }),

  getters: {
    isAuthenticated() {
      return !!this.token
    },
    getAuthorizationHeader(state) {
      return {
        'Authorization': `Bearer ${state.token}`
      }
    }
  },

  actions: {

    async register(name, lastname, email, password) {
      try {
        const response = await fetch(`${import.meta.env.VITE_API_URL}/auth/register`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            name,
            lastname,
            email,
            password
          })
        })
        const result = await response.json()
        if (!result.error) {
          this.token = result.token
          localStorage.setItem('token', this.token)
          router.push({ name: 'home' })
        }
      } catch (error) {
        console.error(error)
      }
    },

    async login(email, password) {
      try {
        const response = await fetch(`${import.meta.env.VITE_API_URL}/auth/login`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email, password }),
        });
        const result = await response.json();
        if (!result.error) {
          this.token = result.token
          localStorage.setItem('token', this.token)
          console.log("Here we go 😎");
          router.push({ name: 'home' })
        }
      } catch (error) {
        console.error("Error:", error);
      }
    },

    logout() {
      this.user = {}
      this.token = ''
      localStorage.removeItem('token')
      router.push({ name: 'login' })
    }
  }

})
