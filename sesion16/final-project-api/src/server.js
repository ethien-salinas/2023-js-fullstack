import 'dotenv/config';
import cors from 'cors';
import express from "express";
import { authorizationMiddleware } from './middleware/authorization';
import { logRequestMiddleware } from './middleware/request-log';
import { traceIdMiddleware } from './middleware/trace-generator';
import { router as routerAuth } from "./router/auth";

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())
app.use(cors())

app.use(traceIdMiddleware)
app.use(logRequestMiddleware)

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.get('/public', (req, res) => {
  res.send('Hello World! 😎')
})
app.get('/protected', authorizationMiddleware, (req, res) => {
  res.send('Hello World! 🤯')
})

app.use('/auth', routerAuth)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})