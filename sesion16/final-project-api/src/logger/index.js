import { createLogger, format, transports } from "winston"
import "winston-daily-rotate-file"

export const logger = createLogger({
  level: process.env.LOGGER_LEVEL,
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZ' }),
    format.printf(({ timestamp, service, level, message }) => `${timestamp} | ${service} | ${level} | ${message}`)
  ),
  defaultMeta: { service: 'API' },
  transports: [
    new transports.DailyRotateFile({
      level: 'error',
      filename: './logs/error-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      utc: false,
    }),
    new transports.DailyRotateFile({
      filename: './logs/application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      utc: false,
    })
  ],
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console());
}
