import { iniciaCon, templateString } from "../string";

const cupcakeipsum = "Sweet roll macaroon chocolate chupa chups topping gummi bears cake apple pie. Carrot cake apple pie candy sugar plum cake marshmallow cake dessert. Candy dessert apple pie icing donut. Apple pie brownie topping liquorice lemon drops danish dessert."

const personas = [
    {
        nombre: "Ethien",
        apellido: "Salinas",
        rol: "admin"
    },
    {
        nombre: "Sharon",
        apellido: "Ramirez",
        rol: "estudiante"
    },
    {
        nombre: "Patty",
        apellido: "Ocampo",
        rol: "estudiante"
    },
]

test('prueba de iniciaCon', () => {
    expect(iniciaCon(cupcakeipsum, 'roll')).toBeFalsy()
    expect(iniciaCon(cupcakeipsum, 'sweet')).toBeFalsy()
    expect(iniciaCon(cupcakeipsum, 'Sweet')).toBeTruthy()
})

test('prueba de template string', () => {
    expect(templateString(personas[0])).toBe("Bienvenido Ethien Salinas, entraste como admin")
    expect(templateString(personas[1])).toBe("Bienvenido Sharon Ramirez, entraste como estudiante")
    expect(templateString(personas[2])).toBe("Bienvenido Patty Ocampo, entraste como estudiante")
})
