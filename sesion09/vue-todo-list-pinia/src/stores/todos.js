import { defineStore } from "pinia";

export const useTodoStore = defineStore('todoStore', {
  state: () => ({
    name: "Ethien Salinas",
    todos: externalTodoList,
    filteredTodos: externalTodoList,
    selectedFilter: 'all'
  }),
  getters: {
    countAll: (state) => state.todos.length,
    countActive: (state) => state.todos.filter(t => !t.isCompleted).length,
    countCompleted: (state) => state.todos.filter(t => t.isCompleted).length,
  },
  actions: {
    // task manipulation
    addTodo(todo) {
      this.todos.push(todo)
      this.filteredTodos = this.todos
    },
    deleteTodo(id) {
      this.todos = this.todos.filter(t => t.id !== id)
      this.filteredTodos = this.todos
    },
    markAsCompleted(id) {
      const todo = this.todos.find(t => t.id === id)
      todo.isCompleted = !todo.isCompleted
    },

    // Filters
    selectAll() {
      console.log('Select All');
      this.selectedFilter = 'all'
      this.filteredTodos = this.todos
    },
    selectCompleted() {
      console.log('Select Completed');
      this.selectedFilter = 'completed'
      this.filteredTodos = this.todos.filter(t => t.isCompleted)
    },
    selectActive() {
      console.log('Select Active');
      this.selectedFilter = 'active'
      this.filteredTodos = this.todos.filter(t => !t.isCompleted)
    },
  }
})

// initial to do list
const externalTodoList = [
  { id: '1ce3e537-b4af-4bcc-a3ce-5cf8eb942f43', title: 'Aprender a aprender', isCompleted: true },
  { id: '362e4a20-0ecb-4826-9bba-65fe79b8601f', title: 'Aprender a programar', isCompleted: false },
  { id: '9aa09360-7ba9-4ac6-8b50-9d69b8a3cea8', title: 'Aprender JS', isCompleted: false },
]